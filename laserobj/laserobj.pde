laser e,f,g;

float x,y;

void setup(){
  size(400,400);
  background(0);
  smooth();

  e = new laser();
  f = new laser();
  g = new laser();
  e.setO(width/2,0.0);
  e.setColor(color(255,0,0),color(255,180,0));
  e.setTransp(190);
  e.load();
  
  f.setO(width/2,0.0);
  f.setColor(color(0,10,240),color(180,180,255));
  f.setTransp(190);
  f.load();
  
  g.setO(width/2,0.0);
  g.setColor(color(0,240,20),color(120,255,120));
  g.setTransp(190);
  g.load();
}

void draw(){
  background(0);

  x = (float) width/2 + cos(radians(millis()/20))*100;
  y = (float) height/2 + sin(radians(millis()/20))*100;
  
  f.setD(x+40,y+40);
  f.beam();
  
  g.setD(x-40,y-40);
  g.beam();
  
  e.setD(x,y);
  e.beam();
}

