class Point{
 
 //vars
 public float x;
 public float y; 
 private color col;
 private color cola;
 private color colb;
 private float sz = 0;
 private float factor = random(0.001,0.05);
 public float init_pos[] = {0,0};
 public float act_dist = random(50,200);
 
 //setup
 Point(int x, int y){
  sz = random(4,10);
  cola = color(random(255),random(255),random(255));
  colb = color(random(255),random(255),random(255));
  col = cola;
  this.init_pos[0] = x;
  this.init_pos[1] = y;
  this.x = x;
  this.y = y;
 }
 
 Point(){
  sz = random(4,10);
  cola = color(random(255),random(255),random(255));
  colb = color(random(255),random(255),random(255));
  col = cola;
  this.init_pos[0] = (int)random(sz,width - sz);
  this.init_pos[1] = (int)random(sz,height - sz);
  this.x = init_pos[0];
  this.y = init_pos[1];
 }
 
 Point(float s, color a, color b){
  sz = s;
  cola = a;
  colb = b;
  col = cola;
  this.init_pos[0] = (int)random(sz,width-sz);
  this.init_pos[1] = (int)random(sz,height-sz);
  this.x = init_pos[0];
  this.y = init_pos[1];
 }
 
 Point(float s, color a, color b, int x, int y){
  sz = s;
  cola = a;
  colb = b;
  col = cola;
  this.init_pos[0] = random(sz,width-sz);
  this.init_pos[1] = random(sz,height-sz);
  this.x = x;
  this.y = y;
 }
 
 //funcs
 void rebuildInitPos(){
   init_pos[0] = random(sz,width-sz);
   init_pos[1] = random(sz,height-sz);
 }
 
 void setFactor(float fc){
   this.factor = fc;
 }
 
 void setActDist(float act){
   act_dist = act;
 }
 
 void setSize(float sz){
   this.sz = sz;
 }
 
 void setColor(color a){
   col = a;
 }

  void setColorB(color a){
   colb = a;
 }
 
 void lerpCol(float percent){
   col = lerpColor(cola,colb,percent);
 }
 
 void Move(float x, float y){
   this.x += x;
   this.y += y;
 }
 
 void MoveBase(float x, float y){
   init_pos[0] += x;
   init_pos[1] += y;
 }
 
 void Scape(int sx, int sy){
   this.Move((-sx+this.x)*factor,(-sy+this.y)*factor);
 }
 
 void Back(){
   this.Move((init_pos[0]-this.x)*factor,(init_pos[1]-this.y)*factor);
 }
 
 //draw
 void Draw(){
  noStroke();
  fill(col);
  ellipse(this.x,this.y,sz,sz);
  //x restrain
  if(this.x < -7){
    this.x = width +7;
    this.rebuildInitPos();
  }
  if(this.x > width + 4){
    this.x = -5;
    this.rebuildInitPos();
  }
  //y restrain
  if(this.y < -7){
    this.y = height +7;
    this.rebuildInitPos();
  }
  if(this.y > height + 4){
    this.y = -5;
    this.rebuildInitPos();
  }
 }
}
