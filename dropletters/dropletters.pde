
PFont fonte;
String buffer = "";
int contadorX = 0, margin = 5, cor = 0, i = 0;
float startTime, timeOut, Y_MOD;

void setup(){
  size(500,200);
  background(0);
  smooth();
  
  fonte = loadFont("CourierNew36.vlw");
  textFont(fonte, 32);
}

void draw(){
 background(0);
 
 timeOut = millis() - startTime;
 
 for(i=1;i<buffer.length();i++){
 Y_MOD = sin(timeOut/100)*i ;
 fill(50,50,0);
 text(buffer.charAt(i),10+i*18,Y_MOD+90+sin((millis()/100)+(i*50))*(i/2));
 cor = int(map(i,0,25,0,255));
 fill(255-cor,cor,0);
 text(buffer.charAt(i),10+i*18,-Y_MOD+98+cos((millis()/100)+(i*50))*(i/2));
 }
}

 void keyPressed()
{
  char k;
  k = (char)key;
  
  if (contadorX==0){
  startTime = millis();
  }
  
  if (contadorX < 26 && key != 8){
    buffer += k;
    contadorX++;
  }else{
     buffer = "";
     contadorX = 0;
  }
 }
