int stars = 10000;
float[] starsx = new float[stars];
float[] starsy = new float[stars];

float[] sizes = new float[stars];

color[] colors = new color[stars];

float[] speeds = new float[stars];

float centrox = 300;
float centroy = 250;

float xoff = 0.0;

void setup() {
  background(0);
  size(1920, 1080);

  centrox = width*0.5f;
  centroy = height*0.5f;

  //fullScreen();

  stroke(255);

  for (int i = 0; i < stars; i++) {
    starsx[i] = random(0, width);
    starsy[i] = random(0, height);
    sizes[i]  = random(0.4, 2);
    speeds[i] = random(0.0008f, 0.01f);
    colors[i] = color(random(50, 255), 0, random(50, 255));
  }

  noiseDetail(8, 0.65);
}

void draw() {
  fill(0, 0, 0, 100);
  rect(0, 0, width, height);

  drawStars();

  //Movimentaçao das estrelas
  xoff = xoff + .1;
  
  float noizex = map(noise(xoff), 0, 1, -1, .5)*10;
  float noizey = map(noise(xoff), 0, 1, -1, .5)*10;
  
  for (int i = 0; i < stars; i++) {
    float movimentox = starsx[i] - centrox;
    float movimentoy = starsy[i] - centroy;

    starsx[i] += random(-2, 2)+movimentox*speeds[i]+noizex;
    starsy[i] += random(-2, 2)+movimentoy*speeds[i]+noizey;

    if ( starsx[i] > width || starsx[i] < 0 ||
      starsy[i] > height || starsy[i] < 0) {
      starsx[i] = random(0, width);
      starsy[i] = random(0, height);
    }
  }
}

void drawStars() {
  for (int i = 0; i < stars; i++) {
    //colors[i] = color(random(0,255),random(0,255),random(0,255));
    stroke(colors[i]);
    strokeWeight(sizes[i]);
    point(starsx[i], starsy[i]);
  }
}
