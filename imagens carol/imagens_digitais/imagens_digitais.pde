PImage image;

color[][] colors;

void setup(){
  background(0);
  
  image = loadImage("central2_sharp.png");
  colors = new color[image.width][image.height];
  
  size(500, 800);
  
  int totalPixels = image.width*image.height;
 
  loadPixels();
  
  //Preprocess pixels
  for(int i=0;i<totalPixels;i++){
     if(brightness(image.pixels[i]) > 180){
       image.pixels[i] = color(255);
     }else{
       image.pixels[i] = color(0);
     }
  }
  
  updatePixels();
  
  copyImage();

  //image(image, 0, 0, width, height);
}

void draw(){
 background(0);
 
 for(int x=0;x<image.width;x++){
     for(int y=0;y<image.height;y++){
       color pixel = colors[x][y];
       
       //White check boundaries
       if(pixel == color(255)){
         color c = getFirstNonWhite(x,y);
         
         //If no colors are different, create new
         if(c == color(255)){
           c = getNewColor(x,y);
         }
         
         stroke(c);
         colors[x][y] = c;
         
         /*strokeWeight(random(2,4));
         if(random(100) > 90)*/
         point(x,y);
       }
       
     }
  }
  copyImage();
}

public void copyImage(){
   for(int x=0;x<image.width;x++){
     for(int y=0;y<image.height;y++){
       colors[x][y] = getPixel(x,y);
     }
  } 
}

public color getFirstNonWhite(int x, int y){
  for(int ix = x-1; ix <= x+1; ix++){
    for(int iy = y-1; iy <= y+1; iy++){
      if(ix == x && iy == y) continue;
      
      //If we are within boundaries
      if(ix >= 0 && ix < image.width){
        if(iy >= 0 && iy < image.height){
          //If the color is not white
          color pixel = colors[ix][iy];
          if(brightness(pixel) != 0 && brightness(pixel) != 255){
            return pixel;
          }
        }
      }
    }
  }
  
  return color(255);
}

public color getNewColor(int x, int y){
  if(random(100) > 50){
    return color(180,100,20);
  }else{
    return color(160, 155, 20);
  }
  //return color(random(255), 20, random(255)*((height - y*1f)/height));
  //return color(random(255),random(255),random(255));
}

color getPixel(int x, int y){
  return image.pixels[y*image.width + x]; 
}
