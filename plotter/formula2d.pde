import javax.script.*;

class formula2d{
  
 String asText;
 
  public formula2d(String formulaText){
    asText = formulaText;
  }
  
  public boolean check(float x, float y){
    scriptParser.engine.put("x",x);
    scriptParser.engine.put("y",y);
    try{
      scriptParser.engine.eval("var ans=("+asText+");");
    }catch(Exception e){
      e.printStackTrace();
      println(e.getMessage());
    }
    boolean ret = (Boolean) scriptParser.engine.get("ans");
    return ret;
  }
  
  public Boolean[][] plot(int halfScreen, int density, float graphSize){
    scriptParser.engine.put("halfScreen",halfScreen);
    scriptParser.engine.put("density",density);
    scriptParser.engine.put("graphSize",graphSize);
    
    try{
      scriptParser.engine.eval(
      " \n"+
      " var rr = new Array();\n"+
      " for(ix=-halfScreen;ix<halfScreen;ix+=density){\n"+
      " rr[ix] = new Array();\n"+
      " for(iy=-halfScreen;iy<halfScreen;iy+=density){\n"+
      "   x = (ix/halfScreen)*graphSize;\n"+
      "   y = (iy/halfScreen)*graphSize;\n"+
      "  \n"+
      "  if("+asText+"){\n"+
      "    rr[halfScreen+((x/graphSize)*halfScreen)][halfScreen+((y/graphSize)*halfScreen)] = 1\n"+
      "  }\n"+
      "  \n"+
      " }\n"+
      " }\n"
      );
    }catch(Exception e){
      e.printStackTrace();
      println(e.getMessage());
    }
    
    sun.org.mozilla.javascript.internal.NativeArray array = (sun.org.mozilla.javascript.internal.NativeArray) scriptParser.engine.get("rr");
    return null;
  }
  
}
