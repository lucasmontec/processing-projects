class system{
  
  private PImage _image;
  private PImage _mask;
  private float _lifeTime;
  private float _lifeTime_variation;
  private float _start_size;
  private float _end_size;
  private float _size_variation;
  private color _start_color;
  private color _end_color;
  private float _color_variation;
  private float _wind_x, _wind_y, _wind_force;
  private float _turbulence;
  private float _air_resist;
  private float _gravity;
  private float _width, _height;
  private float _roll;
  private float _x, _y;
  private int _max_particles;
  private final particle[] _particles;
  
  //construtor
  system(int systemMaxParticles){
    _particles = new particle[systemMaxParticles];
    for(int i=0;i<systemMaxParticles;i++){
      _particles[i] = new particle();
    }
    _max_particles = systemMaxParticles;
    _lifeTime = 1;
    _lifeTime_variation = 0.3;
    _start_size = 10;
    _end_size = 0;
    _start_color = color(255,255,255,255);
    _end_color = color(0,0,255,0);
    _turbulence = 0.2;
    _wind_x = 0;
    _wind_y = 0;
    _wind_force = 0;
    _air_resist = 0.2;
    _roll = 0.1;
  }
  
  //funções
  
  public void setImage(String img){
     _image = requestImage(img);
  }
 
  public void setMask(String msk){
    if(_image != null){
     _mask = requestImage(msk);
     _image.mask(_mask);
    } 
  }
  
  public void setLifeTime(float lt){
    _lifeTime = lt;
  }
  
  public void setLifeTimeVar(float ltv){
    _lifeTime_variation = ltv;
  }
  
  public void setSizes(float start, float end, float var){
    _start_size = start;
    _end_size = end;
    _size_variation = var;
  }
  
  public void setSizes(float both, float var){
    _start_size = both;
    _end_size = both;
    _size_variation = var;
  }
  
  public void setSizes(float novar){
    _start_size = novar;
    _end_size = novar;
    _size_variation =0;
  }
  
  public void setColors(color start, color end){
    _start_color = start;
    _end_color = end;
  }
  
  public void setColorVar(float v){
    _color_variation = v;
  }
  
  public void setWind(float x, float y, float f){
    _wind_x = x;
    _wind_y = y;
    _wind_force = f;
  }
  
  public void setTurbulence(float t){
    _turbulence = t;
  }
  
  public void setAirResist(float r){
    _air_resist = r;
  }
  
  public void setGravity(float g){
    _gravity = g;
  }
  
  public void setSystemSize(float w, float h){
    _width = w;
    _height = h;
  }
  
  public void setRoll(float r){
    _roll = r;
  }
  
  public void setPos(float x, float y){
    _x = x;
    _y = y;
  }
  
  //funções de ação
  public void move(float x, float y){
    this.setPos(_x + x,_y + y);
  }
  
  public void updateTurbulence(){
    _wind_force += random(-_turbulence, _turbulence);
  }
  
  private void printParticleInfo(particle p){
    println("-p_pos: ["+p._x+"] ["+p._y+"]");
    println("-p_start col: r["+red(p._start_color)+"] g["+green(p._start_color)+"] b["+blue(p._start_color)+"] a["+alpha(p._start_color)+"]");
    println("-p_lt: "+p._lifeTime);
    println("-p_gravity: "+p._gravity);
    println("-p_vel: "+p._vel_x+"/"+p._vel_y);
  }
  
  private void printAllInfo(){
    println("-lt: "+_lifeTime);
    println("-ltv: "+_lifeTime_variation);
    println("-sizes: start "+_start_size+" end "+_end_size);
    println("-gravity: "+_gravity);
    println("-air resist: "+_air_resist);
    println("-start col: r["+red(_start_color)+"] g["+green(_start_color)+"] b["+blue(_start_color)+"] a["+alpha(_start_color)+"]");
    println("-pos: ["+_x+"] ["+_y+"]");
    println("-system size: "+_width+"/"+_height);
  }
  
  private color colorVar(color c, float f){
    if(f < 1){
      final float r = red(c)+random(-red(c)*f,red(c)*f);
      final float g = green(c)+random(-green(c)*f,green(c)*f);
      final float b = blue(c)+random(-blue(c)*f,blue(c)*f);
      if(r<=255 && r >= 0 && g<=255 && g >= 0 && b<=255 && b >= 0){
        return color(r,g,b);
      }else{
        return c;
      }
    }else{
      return c;
    }
  }
  
  private void prepareParticle(particle p){
      p.setStart();
      p.setImage(_image);
      p.setMask(_mask);
      p._start_color = colorVar(_start_color, _color_variation);
      p.setSizes(_start_size + random(-_size_variation,_size_variation),_end_size);
      p._gravity = _gravity;
      p._air_resist = _air_resist;
      p._x = _x + random(_width);
      p._y = _y + random(_height);
      if(_lifeTime >= 0){
        p._lifeTime = _lifeTime + random(-_lifeTime_variation,_lifeTime_variation);
      }else{
        p._lifeTime = -1;
      }
  }
  
  private void prepareParticles(){
    for(int i=0;i<_max_particles;i++){
      prepareParticle(_particles[i]);
    }
  }
  
  //spray
  public void spray(float force){
    prepareParticles();
    for(int i=0;i<_max_particles;i++){
      _particles[i]._spawn_x = _x + _width/2;
      _particles[i]._spawn_y = _y + _height/2;
      _particles[i].setVel((_particles[i]._x-_particles[i]._spawn_x)*force,(_particles[i]._y-_particles[i]._spawn_y)*force);
      _particles[i].setAngVel(random(-_roll,_roll));
      //printParticleInfo(_particles[i]);
      //printAllInfo();
      println(_particles[i]._x);
    }
  }
  
  public void show(){
    for(int i=0;i<_max_particles;i++){
      _particles[i].show();
      if(_particles[i]._lifePercent >= 0.9){
        prepareParticle(_particles[i]);
      }
    }
  }
}
