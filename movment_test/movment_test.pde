
float x=0,y=0, angle;

void setup(){
  background(0);
  smooth();
  size(400,400);
  
}

void draw(){
  background(0);
  angle = atan((mouseY-y)/(mouseX-x));
  
  if(key == 'w'){
    x += sin(angle)*0.6;
    y += cos(-angle)*0.6;
  }
  
  ellipse(x,y,5,5);
}
