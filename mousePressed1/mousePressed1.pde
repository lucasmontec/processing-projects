void setup() {
  size(900,600);
  background(0);
  noStroke();
}
  
void draw() {
  //Cada vez que roda, printa um retangulo com 5% de transparencia
  fill(0,0,0,5);
  rect(0,0,width,height);
  
  //Se o mouse estiver pressionado...
  if (mousePressed) {
  //Queria mudar o tamanho da ellipse!!!
    int tamanho=10;
    if(tamanho==100){
      tamanho+=30;}
      
   //...Imprimira ellipse em sequencia, no movimento do mouse
    fill(255);
    ellipse(mouseX, mouseY,tamanho,tamanho);

   
  }}
