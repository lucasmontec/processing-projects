// Game of Life
lifeMatrix lm;

void setup(){
  size(500,500);
  smooth();
  
  lm = new lifeMatrix(27,27);
  lm.randomize();
}

void draw(){
  background(0);
  int old_matrix[][] = lm.matrix;
  lm.calcGame();
  delay(100);
  
  if(mousePressed){
    lm.randomize();
  }
  
  for(int x=0;x<lm._width;x++){
   for(int y=0;y<lm._height;y++){
    if(lm.matrix[x][y]==1){
      fill(180,160,0);
    }else{
      fill(30);
    }
    ellipse(18+x*18,18+y*18,14,14);
   } 
  }
  
}
