class level{
  
  ArrayList<gameButton> buttons;
  int lvl = 0;
  level_info info;
  boolean complete;
  
  public level(level_info info){
    this.info = info;
    buttons = new ArrayList<gameButton>();
    generate();
  }
  
  /*
  *  LEVEL UPDATE
  */
  public void click(){
    //Sum for complete check
    int sum = 0;
    
    for(gameButton b : buttons){
      b.click(); 
      
      //Sum for complete check
      sum += (b.isComplete(buttons)?1:0);
    }
    
    //Check if level is complete
    complete = (sum == buttons.size());
  }
  
  public void update(){
    
  }
  
  public void draw(){
    for(gameButton b : buttons){
      b.draw(); 
      b.update();
    }
    
    if(complete){
      rect(0,0,100,100); 
    }
  }
  /*
  *  LEVEL GENERATION
  */
  
  private void generate(){
    println("Generating level...");
    for(int c=0;c<info.level_buttons;c++){
      println("Creating button ["+c+"]");
      gameButton gb = new gameButton();
      buttons.add(gb);
      gb.setWState(getState(buttons.indexOf(gb)));
      gb.setBounds(50 + c*40  ,10 , 20, 40);
    }
    println("Level generated.");
  }
  
  private State getState(int btn_id){
    println("Random state selection starting...");
    int rnd_slot = 0;
    do{
      rnd_slot = (int)random(info.amount_of_states.length);
    }while(info.amount_of_states[rnd_slot] <= 0);
    info.amount_of_states[rnd_slot]--;
    println("Random state selection: state id selected.");
    
    int meta = 0;
    if(buttons.size() > 2){
      do{
        meta = (int)random(buttons.size());
      }while(meta == btn_id);
      println("Random state selection: state metadata randomized.");
    }else{
      println("Random state selection: state metadata zeroed.");
    }
    
    println("State selected ["+rnd_slot+","+meta+"]");
    println("Random state selection ended.");
    return new State(rnd_slot, meta);
  }
  
}
