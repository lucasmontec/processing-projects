level l;

void setup(){
  l = new level(new level_info(0, 3, new int[]{1,1,0,1,0}));
  smooth();
  size(500,500);
  background(0);
}

void draw(){
  background(0);
  l.draw();
  l.update();
}

void mouseReleased() {
  l.click();
}
