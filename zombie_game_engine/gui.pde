abstract class gui{

  //inner vars
  public color my_col = color(255,255,255);
  public color my_bd_col = color(255,255,255);
  public float my_alpha = 255.0;
  public float my_bd_alpha = 255.0;
  public float my_border = 1.0;

  //public vars
  public float x = 0;
  public float y = 0;
  public float wd = 10;
  public float ht = 10;

  //construtores
  gui(){
    //sem nada
  }

  gui(float x,float y,float w, float h){
    this.x = x;
    this.y = y;
    this.wd = w;
    this.ht = ht;
  }

  //funções sem retorno

  void setWidth(float w){
    this.wd = w;
  }

  void setHeigth(float h){
    this.ht = h;
  }

  void setSize(float w, float h){
    this.wd = w;
    this.ht = h;
  }

  void sX(float x){
    this.x = x;
  }

  void sY(float y){
    this.y = y;
  }

  void setPos(float x, float y){
    this.x = x;
    this.y = y;
  }

  void setColor(color a){
    my_col = a;
  }

  void setColor(float r, float g, float b){
    my_col = color(r,g,b);
  }

  void setAlpha(float a){
    my_alpha = a;
  }

  void setBorder(float sz){
    my_border = sz;
  }

  void setBorderColor(color cc){
    my_bd_col = cc;
  }

  void setBorderColor(float r, float g, float b){
    my_bd_col = color(r,g,b);
  }

  void setBorderAlpha(float a){
    my_bd_alpha = a;
  }

  //funções de ação

  void Dim(float val){
    if ((my_alpha + val) <= 255){
      my_alpha += val;
    }else{
      my_alpha = 255;
    }
  }

  void FullDim(float val){
    if ((my_alpha + val) <= 255){
      my_alpha += val;
      
      if ((my_bd_alpha + val) <= 255){//border
      my_bd_alpha += val;
      }
      
    }else{
      my_alpha = 255;
    }
  }
 
   void FullDimTo(float val,float MX){
    if ((my_alpha + val) <= MX){
      my_alpha += val;
      
      if ((my_bd_alpha + val) <= MX){//border
      my_bd_alpha += val;
      }
      
    }else{
      my_alpha = MX;
    }
  }
 
  void Fade(float val){
    if ((my_alpha - val) >= 0){
      my_alpha -= val;
    }else{
      my_alpha = 0;
    }
  }
  
  void FullFade(float val){
    if ((my_alpha - val) >= 0){
      my_alpha -= val;
      
      if ((my_bd_alpha - val) >= 0){//border
      my_bd_alpha -= val;
      }
      
    }else{
      my_alpha = 0;
    }
  }
  
  void Move(float x, float y){
    this.x += x;
    this.y += y;
  }

  boolean MouseOver(){
    if (mouseX > this.x && mouseX < (this.x+this.wd)){
      if (mouseY > this.y && mouseY < (this.y+this.ht)){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  int MouseButton(){
    if (this.MouseOver()){
      if (mouseButton == LEFT){
        return 1;
      }else{
        if (mouseButton == RIGHT){
          return 2;
        }else{
          return -1;
        }
      }
    }else{
      return -1;
    }
  }

  boolean Pressed(){
    if(mousePressed && this.MouseOver()){
      return true;
    }else{
      return false;
    }
  }

  abstract void Draw();

}

