public class Button extends gui{

  //------vars
  private int formato = 1;
  private String txt = "";
  private int txt_sz = 16;
  private color col_over = color(255,255,255);
  private color col_dwn = color(255,255,255);
  private color col_txt = color(255,255,255);
  PFont a;

  //construtores

  Button(){
    a = loadFont("ArialNarrow-14.vlw");
  }

  Button(float x,float y){
    this.x = x;
    this.y = y;
    a = loadFont("ArialNarrow-14.vlw");
  }

  Button(float x,float y, float wd, float ht){
    this.x = x;
    this.y = y;
    this.wd = wd;
    this.ht = ht;
    a = loadFont("ArialNarrow-14.vlw");
  }

  //funcoes sem retorno

  void setColorTxt(color tt){
    col_txt = tt;
  }

  void setColorTxt(float r,float g,float b){
    col_txt = color(r,g,b);
  }

  void setColorDown(color dn){
    col_dwn = dn;
  }

  void setColorDown(float r,float g,float b){
    col_dwn = color(r,g,b);
  }

  void setColorOver(color ov){
    col_over = ov;
  }

  void setColorOver(float r,float g,float b){
    col_over = color(r,g,b);
  }

  void setText(String txt){
    this.txt = txt;
  }

  void setTextSize(int sz){
    this.txt_sz = sz;
  }

  void setFont(PFont n){
    this.a = n;
  }

  void setShape(String s){
    if(s == "rect"){
      formato = 1;
    }
    else{
      formato = 2;
    }
  }

  //funcoes de acao
  public void Draw(){
    
    //funcionalidade
       //borda
      if (this.my_border <= 0){
        noStroke();
      }
      else{
        stroke(my_bd_col,my_bd_alpha);
        strokeWeight(my_border);
      }

      if (this.MouseOver()){
        if(!this.Pressed()){
          fill(col_over,my_alpha);
        }
        else{
          fill(col_dwn,my_alpha);
        }
      }
      else{
        fill(my_col,my_alpha);
      }
      
    //desenha o botao
    if (formato==1){ //retangulo
      //retangulo
      rect(x,y,wd,ht);
    }else{//ellipse
      //bola
      ellipseMode(CENTER);
      ellipse(x+(wd/2),y+(ht/2),wd,ht);
    }
    
    //desenha o texto
    if (txt != "" && a != null){
    fill(col_txt);
    textAlign(CENTER);
    textFont(a, txt_sz);
    text(txt, x+1+(wd/2), y+4+(ht/2));
    }
  }
  
}


