
bola bola1, bola2;

void setup(){
  size(200,200);
  background(0,10,40);
  smooth();
  
  bola1 = new bola();
  bola2 = new bola();
}

void draw(){
  background(0,10,40);
  
  bola1.setCenter(bola1.x()+random(-1,1),bola1.y()+random(-1,1));
  bola2.setCenter(bola2.x()+random(-1,1),bola2.y()+random(-1,1));
  
  bola1.show();
  bola2.show();
  
  if (bola1.getDistance(bola2) < 100){
    bola1.lineTo(bola2.x(),bola2.y());
  }
}
