class Level{
  
  public int level = 1;
  public float playerScore = 0;
  public long levelDuration = 5000;
  public long levelStart = 0;
  public long mSpawnTime = 1000;
  public long mLastSpawn = 0;
  public long mSpawnTimeDec = 3;
  ArrayList<monster> monsters = new ArrayList<monster>();
  ArrayList<monster> rmonsters = new ArrayList<monster>();
  private ship player;
  public final int max_Monsters = 50;
  PFont fonte;
  
  public Level(ship s){
    player = s;
    fonte = loadFont("CourierNew36.vlw");
    textFont(fonte, 12);
  }
  
  public void resetLevel(){
    level = 1;
    playerScore = 0;
    monsters.clear();
  }
  
  public void update(){
    
    //Only update if the player is alive
    if(player.alive){
      
      //process lvel
      if(millis()-levelStart > levelDuration){
        //Increase level
        level++;
        
        //Calc new duration
        levelDuration = 10000 + level*500;
        
        //New spawn time
        mSpawnTime -= mSpawnTimeDec;
        
        //Store start
        levelStart = millis();
      }
      
      //Spawn monsters
      if(millis()-mLastSpawn > mSpawnTime && monsters.size() < max_Monsters){
        
        //Select x's and y's
        float x = 0;
        float y = 0;
        if(random(100) > 50){
          if(random(100) > 50){
            x = -10;
          }else{
            x = width+10;
          }
          y = random(0,height);
        }else{
          x = random(0,width);
          if(random(100) > 50){
            y = -10;
          }else{
            y = height+10;
          }
        }
        
        //Add some green monsters over time
        if(level > 15){
          if(random(100) > 85){
            monsters.add(
              //Spawn the basic monster
              new greenMonster(level,x,y,player)
            );
          }else{
            monsters.add(
            //Spawn the basic monster
            new monster(level,x,y,player)
            );
          }
        }else{
          //Spaw mosnter
          monsters.add(
            //Spawn the basic monster
            new monster(level,x,y,player)
          );
        }
        
        //store last
        mLastSpawn = millis();
      }
      
      //Update monstrs
      for(monster m : monsters){
        if(m.alive){
          m.update();
        }else{
          playerScore += m.score;
          player.money += m.money;
          popUpManager.pops.add(new popUp(m.position.x,m.position.y,color(240,240,30),"$"+m.money,570));
          rmonsters.add(m);
        }
      }
      
      if(rmonsters.size()>0){
        for(monster m : rmonsters){
          monsters.remove(m);
          m = null;
        }
        rmonsters.clear();
      }
    
    }
  }
  
  
  public void draw(){
    //Draw monstars
    for(monster m : monsters){
      m.draw(); 
    }
    
    //Draw level
    fill(255);
    text("Level: "+level,8,18);
    text("Score: "+ceil(playerScore),8,32);
    fill(50,240,50);
    text("Money: "+player.money,8,46);
    
    if(player.alive){
      stroke(0,0,0,0);
      fill(10,15,100);
      rect((width-150)/2,18,150,8);
      fill(60,20,250);
      rect((width-150)/2,18,150*((millis()-levelStart)*1f/levelDuration*1f),8);
      //text("Level percent: "+floor(((millis()-levelStart)*1f/levelDuration*1f)*100)+"%",8,64);
    }else{
      fill(255,40,60);
      text("Game over!",8,64);
    }
    
    //Draw popUps
    popUpManager.draw();
  }
}
