ship s;
Level level;

void setup(){
  size(1024,768);
  smooth();
  
  s = new ship();
  level = new Level(s);
}

void draw(){
  background(0);
 
  if(s.alive){
    if(mousePressed){
      s.shot();
    }
    
    if(keyPressed){
      s.keyPressed(key); 
    }
    
    s.draw();
    s.update();
  }
  
  level.update();
  level.draw();
  
  worldBullets.updateBullets();
}
