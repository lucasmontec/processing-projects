ship s;
Level level;

/*
Controls:
  r - repair        |cost:50   currHull+30
  h - upgrade hull  |cost:900  hull+100
  p - upgrade power |cost:1000 dmg+2
*/

void setup(){
  size(1024,768);
  smooth();
  
  s = new ship();
  level = new Level(s);
}

void draw(){
  background(0);
 
  if(s.alive){
    if(mousePressed){
      s.shot();
    }
    
    if(keyPressed){
      s.keyPressed(key); 
    }
    
    s.draw();
    s.update();
  }
  
  level.update();
  level.draw();
  
  worldBullets.updateBullets();
}
