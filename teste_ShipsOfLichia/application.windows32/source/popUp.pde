class popUp{
  
  public String text;
  public color col;
  public float x,y;
  public boolean alive = true;
  private long startTime=0;
  private int duration;
  private float t;
  
  public popUp(float x, float y, color c, String txt, int dur){
    text = txt;
    col = c;
    this.x = x;
    this.y = y;
    startTime = millis();
    duration = dur;
  }
  
  public void draw(){
    //Only draw alive
    if(alive){
      //Calc time
      t = millis() - startTime;
      
      //Draw the tip
      fill(lerpColor(col,color(0,0,0,0),constrain(t*1f/duration*1f,0,1)));
      text(text,x,y); 
      
      //Up text
      y -= 0.001f*(2000-duration);
      
      //Time check
      if(t > duration){
        alive = false;
      }
    }
  }
  
}
