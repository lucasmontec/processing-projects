class turret{
  
  public float rotation;
  public ship owner;
  public final long fireDelay = 100;
  public long lastFired = 0;
  public final int maxAmmo = 27;
  public int damage = 5;
  
  public float ahelper = 0;
  
  public turret(ship o){
    owner = o;
    rotation = 0;
  }
  
  public void shot(){
    //Fire
    if(millis()-lastFired > fireDelay && worldBullets.bullets.size() < maxAmmo){
      //fire a bullet
      worldBullets.bullets.add(
      new bullet(
      owner.position.x+sin(rotation-PI/2)*12,
      owner.position.y+cos(rotation-PI/2)*12,
      owner.position.x+sin(rotation-PI/2)*20,
      owner.position.y+cos(rotation-PI/2)*20,
      damage)
      );
      
      lastFired = millis();
    }
  }
  
  public void setDamage(int d){
    damage = d; 
  }
  
  public float lerpAngle(float a, float fac){
    float delta = a-rotation;
    
    if(abs(delta) > PI){
       delta = (delta > 0 ? (delta - 2*PI) : (delta + 2*PI));
    }
    
    return delta*fac;
  }
  
  public void update(){
    ahelper = -atan2(owner.position.y-mouseY,owner.position.x-mouseX);
    rotation += lerpAngle(ahelper,.07);
  }
  
  public void draw(){
    stroke(255,60,60);
    strokeWeight(4);
    line(owner.position.x,owner.position.y,
    owner.position.x+sin(rotation-PI/2)*12,owner.position.y+cos(rotation-PI/2)*12);
    strokeWeight(1);
  }
}
