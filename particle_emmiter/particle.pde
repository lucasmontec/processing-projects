class particle{
 
 private float x = 0;
 private float y = 0;
 private float my_angle = 0;
 private float my_size = 0;
 private PImage my_image;
 private color my_color;
 private float my_transp;
 private PImage my_mask;
 private float em_x;
 private float em_y;
 
 particle(){
  x = random(0,width);
  y = random(0,height);
  my_color = color(random(255),random(255),random(255));
  my_angle = random(0,360);
  my_size = random(4,60);
  my_transp = random(40,150); 
 }
 
 particle(PImage img){
  x = random(0,width);
  y = random(0,height);
  my_angle = random(0,360);
  my_size = random(4,60); 
  my_image = img; 
 }
 
  particle(PImage img, PImage msk){
  x = random(0,width);
  y = random(0,height);
  my_angle = random(0,360);
  my_size = random(4,60); 
  my_image = img; 
  my_mask = msk;
 }
 
 particle(float x, float y){
  this.x = x;
  this.y = y;
  my_angle = random(0,360);
  my_size = random(4,60);
  my_transp = random(40,150);
 }
 
 particle(PImage img, float x, float y){
  this.x = x;
  this.y = y;
  my_angle = random(0,360);
  my_size = random(4,60); 
  my_image = img;   
 }
 
 //---------------------------------funções------------------------
 
 void eX(float x){
   em_x = x;
 }
 
 void eY(float y){
   em_y = y;
 } 
 
 float eX(){
  return em_x; 
 }
 
 float eY(){
  return em_y; 
 }
 
 void setImage(PImage img){
   my_image = img;
 }
 
 void setImage(String path){
   my_image = loadImage(path);
 }
 
 void setMask(PImage msk){
  my_mask =  msk;
 }
 
 void freeMask(){
   my_mask = null;
 }
 
 void freeImage(){
  my_image = null; 
 }
 
 void setTransp(float tr){
   my_transp = tr;
 }
 
 void dimm(float v){
  if (my_transp - v > 0){
  my_transp -= v; 
  }
 }
 
 void setTransp(float mn, float mx){
   if (mn<mx){
   my_transp = random(mn,mx);
   }
 }
 
 void setColor(color a){
   my_color = a;
 }
 
 void setColor(float r,float g,float b){
   my_color = color(r,g,b);
 }
 
 void setSize(float sz){
   my_size = sz;
 }
 
 void setSize(float mn, float mx){
   my_size = random(mn,mx);
 }
 
 void setAngle(float ang){
   my_angle = ang;
 }
 
 void setPos(float x, float y){
   this.x = x;
   this.y = y;
 }
 
 void doRotation(float ang){
   my_angle += ang;
 }
 
 void x(float x){
   this.x = x;
 }
 
 void y(float y){
  this.y = y; 
 }
 
 float x(){
   return x;
 }
 
 float y(){
   return y;
 }
 
 color getColor(){
  return my_color; 
 }
 
 float getAngle(){
  return my_angle; 
 }
 
 float getSpeed(){
  float oldx = x, oldy = y;
  delay(1);
  float nwx = x, nwy = y;
  return ((nwx-oldx)+(nwy-oldy)/2); 
 }
 
 float getDistance(particle p){
   return sqrt( pow((this.x-p.x()),2) + pow((this.y-p.y()),2) );
 }
 
 float getDistance(float x, float y){
   return sqrt( pow((this.x-x),2) + pow((this.y-y),2) );
 }
 
 //--------------------------------system funcs---------------------------------------
 
 void startMask(){
   if(my_image != null && my_mask != null){
   my_image.mask(my_mask); 
  } 
 }
 
 void show(){
  if (my_image != null){
   pushMatrix();
   rotate(my_angle);
   image(my_image,x-(my_image.width/2),y-(my_image.height/2),my_size,my_size);
   popMatrix();
  }else{
   fill(my_color,my_transp);
   noStroke();
   ellipse(x,y,my_size,my_size);
  }
 }
 
 
}
