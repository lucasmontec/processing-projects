int px[] = new int[5];
int py[] = new int[5];
int estado[] = new int[5];

void setup(){
  background(0);
  noStroke();
  size(600,500);
  
  estado[0] = 1;
  estado[1] = 1;
  estado[2] = 1;
  estado[3] = 1;
  estado[4] = 1;
  
  px[0] = 0;
  py[0] = 0;
  
  px[1] = 50;
  py[1] = 50;
  
  px[2] = 100;
  py[2] = 100;
  
  px[3] = 150;
  py[3] = 150;
  
  px[4] = 200;
  py[4] = 200;
}
void draw(){
  fill(0,0,0,5);
  rect(0,0,600,500);
  
  fill(255,0,0);
  quadrado(0);
  
  fill(0,255,0);
  quadrado(1);
  
  fill(0,0,255);
  quadrado(2);
  
  fill(255,0,255);
  quadrado(3);
  
  fill(255,255,0);
  quadrado(4);
}

void quadrado(int i){
  rect(px[i],py[i],50,50);
  
  //Ir pra esquerda
  if(estado[i] == 1 && px[i] <= 550-(i*50)){
    px[i] = px[i]+10;
  }else if(estado[i] == 1){
    estado[i] = 2;
  }
 
  //Ir pra baixo
  if(estado[i]==2 && py[i] <= 450-(i*50)){
    py[i] = py[i]+10;
  }else if(estado[i] == 2){
    estado[i] = 3;
  }
  
  //Ir pra direita
  if(estado[i]==3 && px[i] >= (i*50)){
    px[i] = px[i]-10;
  }else if(estado[i] == 3){
    estado[i] = 4;
  }
  
  if(estado[i]==4 && py[i] >= (i*50)){
    py[i] = py[i]-10;
  }else if(estado[i] == 4){
    estado[i] = 1;
  }
}
