float timer=0;

void setup(){
  background(0);
  size(400,400);
  smooth();
  strokeWeight(4);
}

void draw(){
  //background(0);
  timer+=0.01;
  for(int i=-150; i<150; i++){
   stroke(abs(sin(timer))*255,0,abs(cos(timer))*255,abs(sin(timer))*255);
   point((width/2)+i,(height/2)+(sin(timer))*(i*i));
   stroke(abs(cos(timer))*255,0,abs(sin(timer))*255,abs(sin(timer))*255);
   point((height/2)+(sin(timer))*(i*i),(width/2)+i); 
  }
}
