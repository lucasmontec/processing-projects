class especie{
 
 //Variaveis que definem o tipo de especie em questão
 public String tipo = "animal"; // Varia de animal para vegetal (superficial)
 public String nome = "base-class-especie"; // Pode ser alterado ou gerado automaticamente
 public float mov_rate = 1; // Multiplicador de movimentação
 public float fator_mutante = 0; //Fator simulador do sintoma evolutivo
 public float tamanho = 5;
 
 //Variaveis que definem a relação com o meio todas variam de -1 a 1
 public float amb_aliment = 0; // -1 carnivoro e 1 herbivoro
 public float amb_humidade = 0; // -1 húmido e 1 seco
 public float amb_temp = 0; // -1 quente e 1 frio
 public float amb_ar = 0; // -1 poluido e 1 limpo
 
 //Construtores
 public especie(){
   RandomizaFatores();
 }
 
 public especie(String tip, float mr){
   tipo = tip;
   mov_rate = mr;
 }
 
 public especie(String tip, float mr, float amba, float ambh, float ambt, float ambr){
   tipo = tip;
   mov_rate = mr;
   amb_aliment = amba;
   amb_humidade = ambh;
   amb_temp = ambt;
   amb_ar = ambr;
 }
 
 public especie(especie w){
   tipo = w.tipo;
   mov_rate = w.mov_rate;
   amb_aliment = w.amb_aliment;
   amb_humidade = w.amb_humidade;
   amb_temp = w.amb_temp;
   amb_ar = w.amb_ar;
   fator_mutante = w.fator_mutante;
   tamanho = w.tamanho;
 }
 
 //Corpo funcional do objeto
 
 //Informações pessoais - Retornos
 public String pegaNome(){
   return nome;
 }
 
 public String pegaTipo(){
   return tipo;
 }
 public float pegaMovRate(){
   return mov_rate; 
 }
 public float pegaFatorMut(){
   return fator_mutante;
 }
 
 public float pegaCaracteristicaAmbiental(char input){
   switch(input){
     case 'a':
       return amb_aliment;
     case 'h':
       return amb_humidade;
     case 't':
       return amb_temp;
     case 'r':
       return amb_ar;
     default:
       return 0;
   }
 }
 
 //Informações pessoais - Envio
 public void setaTipo(String t){
   tipo = t;
 }
 
 public void setaNome(String n){
   if(n != ""){
     nome = n;
   }else{
     nome = geraNome();
   }
 }
 
 public void setaMovimento(float mv){
   mov_rate = mv;
 }
 
 public void setaFatorMutante(float fm){
   fator_mutante = fm;
 }
 
 //Ambientais - Envio
 public void setaAlimetacao(float a){
   amb_aliment = a;
 }
 
 public void setaHumidade(float h){
  amb_humidade = h; 
 }
 
 public void setaTemperatura(float t){
  amb_temp = t; 
 }
 
 public void setaAr(float ar){
   amb_ar = ar;
 }
 
 //Funçoes de ação
 public void RandomizaFatores(){
   if(random(10)>5){
     tipo = "animal";
   }else{
     tipo = "vegetal";
   }
   mov_rate = random(0.5,1.2);
   amb_aliment = random(-1.0,1.0);
   amb_humidade = random(-1.0,1.0);
   amb_temp = random(-1.0,1.0);
   amb_ar = random(-1.0,1.0);
   setaNome("");
 }
 
 //Corpo privado
 
 //Codigo de geração de nomes automatico
 private final String[] carn_tags = {
   "car",
   "eat",
   "arne",
   "mea",
   "raw"
 };
  private final String[] herb_tags = {
   "herb",
   "ivo",
   "inha",
   "far",
   "cute"
 };
 private final String[] hot_tags = {
   "fo",
   "go",
   "la",
   "ir",
   "move"
 };
 private final String[] cold_tags = {
   "ge",
   "lo",
   "fri",
   "tin",
   "lho"
 };
 private final String[] hum_tags = {
   "ag",
   "agu",
   "gua",
   "sc",
   "quiz"
 };
 private final String[] sec_tags = {
   "des",
   "sert",
   "to",
   "dry",
   "har"
 };
 private String geraNome(){
   int tagSize = 5;
   String buffer = "";
   //junta uma tag para o tipo de alimentação
   if(amb_aliment > 0){
     buffer += herb_tags[int(random(tagSize))];
   }else{
     buffer += carn_tags[int(random(tagSize))];
   }
   
   //junta uma tag para o tipo de temperatura
   if(amb_temp > 0){
     buffer += cold_tags[int(random(tagSize))];
   }else{
     buffer += hot_tags[int(random(tagSize))];
   }
   
   //junta uma tag para o tipo de humidade
   if(amb_humidade > 0){
     buffer += sec_tags[int(random(tagSize))];
   }else{
     buffer += hum_tags[int(random(tagSize))];
   }
   
   return buffer;
 }
 
}
