Particle[][] matrix;
Particle[][] nextMatrix;

int brushSize = 40;
Particle currentBrush = new Sand();
int dtMillis;
int dt;

void setup() {
  matrix = new Particle[800][600];
  nextMatrix = new Particle[800][600];
  
  size(800,600);
  background(0);
  noSmooth();
}

void draw() {
  dtMillis = millis();
  background(0);
  paintBrush();
  
  for(int y=0;y<600;y++){
    for(int x=0;x<800;x++){
      if(matrix[x][y] != null){
        matrix[x][y].update(dt, x, y, matrix, nextMatrix);
      }
    }
  }
  
  for(int y=0;y<600;y++){
    for(int x=0;x<800;x++){
      matrix[x][y] = nextMatrix[x][y];
      nextMatrix[x][y] = null;
      
      if(matrix[x][y] != null){
        stroke(matrix[x][y].getColor());
        point(x,y);
      }
    }
  }
  
  println(dt);
  dt = millis() - dtMillis;
}

void keyPressed(){
  if(key == 'w'){
     currentBrush = new Water();
  }
  
  if(key == 's'){
     currentBrush = new Sand();
  }
}

void paintBrush(){
  
  if(!mousePressed){
    return; 
  }
  
  /*
  if(matrix[mouseX][mouseY] == null){
    matrix[mouseX][mouseY] = currentBrush.getNew();
  }
  */
  
  int halfBrush = (int)(brushSize*0.5f);
  for(int brushY=-halfBrush; brushY<halfBrush; brushY++){
    for(int brushX=-halfBrush; brushX<halfBrush; brushX++){
      int x = brushX + mouseX;
      int y = brushY + mouseY;
      
      if(x < 0) x = 0;
      if(x >= matrix.length-1) x = matrix.length-1;
      
      if(y < 0) y = 0;
      if(y >= matrix[0].length-1) y = matrix[0].length-1;
      
      if(matrix[x][y] == null && x % 4 == 0 && y % 4 == 0){
        matrix[x][y] = currentBrush.getNew();
      }
    }
  }
}
