
abstract class Particle {
  int type;
  float velocityX;
  float velocityY;
  
  public abstract int getLifetime();
  public abstract int getType();
  public abstract color getColor();
  public abstract void update(int dt, int x, int y, Particle[][] matrix, Particle[][] nextMatrix);
  public abstract Particle getNew();
}
