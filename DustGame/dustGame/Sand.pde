public class Sand extends Particle {
  
  public int getLifetime(){
    return -1;//infinite
  }
  
  public int getType() {
    return 1; 
  }
  
  public color getColor(){
    return color(190, 170, 70);
  }
    
  public void update(int dt, int x, int y, Particle[][] matrix, Particle[][] nextMatrix){
    velocityY += Physics.Gravity;
    
    int desiredY = (int)(y + velocityY * dt);
    
    if(desiredY >= matrix[0].length){
      desiredY = matrix[0].length-1;
    }
    
    int selectedY = y;
    int selectedX = x;
    
    for(int yTravel=y+1; yTravel <= desiredY; yTravel++){
        //Bottom
        if(nextMatrix[x][yTravel] == null && matrix[x][yTravel] == null){
          selectedY = yTravel;
          continue;
        }
        
        //Bottom sides
        if(selectedX-1 > 0 && nextMatrix[selectedX-1][yTravel] == null && matrix[selectedX-1][yTravel] == null){
          selectedX = selectedX-1;
          selectedY = yTravel;
          continue;
        }
        
        if(selectedX+1 < matrix.length && nextMatrix[selectedX+1][yTravel] == null && matrix[selectedX+1][yTravel] == null){
          selectedX = selectedX+1;
          selectedY = yTravel;
          continue;
        }
    }
    
    nextMatrix[selectedX][selectedY] = this;
  }

  public Particle getNew(){
    return new Sand(); 
  }
}
