public class Water extends Particle {
  
  public int getLifetime(){
    return -1;//infinite
  }
  
  public int getType() {
    return 2; 
  }
  
  public color getColor(){
    return color(60, 70, 200);
  }
    
  public void update(int dt, int x, int y, Particle[][] matrix, Particle[][] nextMatrix){
    if(y+1 < matrix[0].length){
      //Bottom
      if(matrix[x][y+1] == null && nextMatrix[x][y+1] == null){
        nextMatrix[x][y+1] = this;
        return;
      }
      
      //Bottom sides
      if(x-1 > 0 && matrix[x-1][y+1] == null && nextMatrix[x-1][y+1] == null){
        nextMatrix[x-1][y+1] = this;
        return;
      }
      
      if(x+1 < matrix.length && matrix[x+1][y+1] == null && nextMatrix[x+1][y+1] == null){
        nextMatrix[x+1][y+1] = this;
        return;
      }
    }
    
    //Side to side
    if(x-1 > 0 && matrix[x-1][y] == null && nextMatrix[x-1][y] == null){
      nextMatrix[x-1][y] = this;
      return;
    }
    
    if(x+1 < matrix.length && matrix[x+1][y] == null && nextMatrix[x+1][y] == null){
      nextMatrix[x+1][y] = this;
      return;
    }
    
    nextMatrix[x][y] = this;
  }

  public Particle getNew(){
    return new Water(); 
  }
}
