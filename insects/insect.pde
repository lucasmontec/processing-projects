class insect{
  
 public float ang = 0;
 public float s = 5;
 public float x = 0;
 public float y = 0;
 public int rebeldia = 0;
 public color col = color(255,255,255,255);
 
 //constructors
 insect(){
   ang = random(360);
   rebeldia = int(random(100));
   s = random(8,18);
   x = width/2;
   y = height/2;
 }
 
 insect(float tx, float ty){
   x = tx;
   y = ty;
   ang = random(360);
   rebeldia = int(random(100));
   s = random(8,18);
 }
 
 insect(float tx, float ty, int sz){
   x = tx;
   y = ty;
   ang = random(360);
   rebeldia = int(random(0,100));
   s = sz;
 }
 
 //funçoes
 void setSize(int sz){
   s = sz;
 }
 
 void setColor(color coli){
   col = coli;
 }
 
 void setPos(float xi, float yi){
   x = xi;
   y = yi;
 }
 
 void setRebel(int rebeldia){
   this.rebeldia = rebeldia;
 }
 
 void setAngle(float angi){
   ang = angi;
 }
 
 void addAngle(float f){
   ang += f;
 }
 
 //açoes
 void Move(float xm, float ym){
   this.setPos(this.x+xm,this.y+ym);
 }
 
 void MoveForward(int speed){
   this.Move(sin(ang)*speed,cos(ang)*speed);
 }
 
 void LerpCol(color colA, color colB, float amt){
   col = lerpColor(colA, colB, amt);
 }
 
 void IntersectAngle(insect i,float factor){
   float deltaAng = (i.ang-ang);
   this.addAngle(deltaAng*factor);
 }
 
 float InsectDist(insect i){
   return dist(this.x, this.y, i.x, i.y);
 }
 
 //drawing
 void show(){
   //stuff
   strokeWeight(1.2);
   stroke(0);
   fill(col);
   
   //body
   ellipse(x,y,s,s);
   strokeWeight(0.8);
   line(x,y,x+sin(ang)*s,y+cos(ang)*s);
   
   //defaults
   fill(255);
   strokeWeight(1);
 }
 
}
