class starfield{
  
 private int amt;
 private float sz; 
 private float vel;
 private particle pArray[] = new particle[400];
 
 starfield(){
  amt = (int)random(100,400);
  sz = random(1,4);
  vel = random(1,15);
  for(int i=0;i<400;i++){
    pArray[i] = new particle();
  }
 }
 
 void setAmmount(int amt){
   this.amt = amt;
 }
 
 void setSize(float sz){
   this.sz = sz;
 }
 
 void setVel(float vel){
   this.vel = vel;
 }
 
 void setSize(float mn, float mx){
   this.sz = random(mn,mx);
 }
 
 void loadParticles(){
   for (int i=0;i<amt;i++){
     pArray[i].setSize(sz/10,sz); 
     pArray[i].setColor(color(random(170,255)));
     pArray[i].setTransp(255);
     pArray[i].x(random(0,width));
     pArray[i].y(random(-height,-1));
   }
 }
 
 private void loadParticle(particle p){
     p.setSize(sz/10,sz); 
     p.setColor(color(random(170,255)));
     p.setTransp(255);
     p.x(random(0,width));
     p.y(random(-height,-1));
 }
 
 void show(){
  for (int i=0;i<amt;i++){
    
    if(pArray[i].y() < height + 4){
     pArray[i].move(0,random(0.1,vel)); 
     pArray[i].show();   
    }else{
     loadParticle(pArray[i]); 
    }
    
  }
 } 
  
  
  
}
