class ponto
{
 
 private double Pos[] = new double[2];
  
 ponto(){
   Pos[0] = 0;
   Pos[1] = 0;
 }
 
 ponto(double x, double y){
   Pos[0] = x;
   Pos[1] = y;
 }
 
 double getPos(int p){
   if (p<2 && p>-1){
   return Pos[p];
   }else{
   return 0;
   }
 }
 
 void setPos(double x, double y){
   Pos[0] = x;
   Pos[1] = y;
 }
 
 void setPos(int x, int y){
   Pos[0] = (double)x;
   Pos[1] = (double)y;
 }
 
 void setPos(){
   Pos[0] = 0;
   Pos[1] = 0;
 }
 
 double getDistance(ponto p){
   return sqrt(pow((float)(Pos[0] - p.getPos(0)),2.0f) + pow((float)(Pos[1] - p.getPos(1)),2.0f));
 }
}
